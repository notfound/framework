Auteurs :

    WEBER Guillaume
    BARRE David

Lien vers Bitbucket :

    https://bitbucket.org/notfound/framework/

Lien vers Webetu :

    https://webetu.iutnc.univ-lorraine.fr/~weber63u/Sandwitch-Framework/FRAMEWORK_CSS/

Descriptif des choix et réalisations du Framework :

    Le framework est divisé en plusieurs modules : Typographie, tableaux, boutons, alertes, composants, navbar, breadcrumb et pager.

    Ces différents modules sont présentés en liens cliquables dans la navbar situé en haut du fichier render.html.

TYPOGRAPHIE :

     Dans un premier temps il a été nécessaire d'élaborer la typographie de notre framework.

    Nous avons effectué dans l'ordre :

     Les titres : Présenté des balises <h1> à <h6>. Nous avons définit aux titres des tailles standards.

     Les sous-titres : Concernant les sous-titres, nous avons utilisé une balise small qui contient la propriété "font-size : smaller". Ceci nous permet d'obtenir des tailles proportionnelles.  

     Les paragraphes : Pour les paragraphes vont avons simplement respecté les paragraphes standard contenant une marge du bas de 5px.

     Les balises de texte inline :

        Mark qui permet de metre en avant les élements important, qui est surligné d'une couleur jaune pour mettre en avant l'élement important.

        Del qui permet de barrer des zones de texte.

        Ins qui permet d'indiquer des élements ajoutés au texte et qui est surligné d'une couleur représentative.

        U qui permet de souligner des élements.

TABLEAUX:

    Concernant ces derniers, nous avons décidé de zébrer une ligne sur deux, avec en background-color une teinte de gris. Lorsque le curseur passe sur une des lignes non zébrés, un hover s'applique et la ligne se met de la même couleur que les lignes zébrés.

BOUTONS:

    Concernant les boutons nous avons décidé d'y affecter un design simple, avec des border-radius et un hover pour donner à l'utilisateur un sentiment d'interaction.

ALERTES:

    Concernant les alertes, nous avons décider d'appliquer les différentes couleurs représentatives des situations qui peuvent s'appliquer à l'utilisateur : vert pour succès, rouge pour erreur, orange pour warning, et bleu pour information.

COMPOSANTS:

    Les composants ici comportent à la fois les headers et les jumbotrons:

    Les headers :

    Ces derniers sont composés d'un titre qui est suivi d'un paragraphe.

    Jumbotron:

    Ces derniers ont la même compositions que le header à la seule différence près qu'il dispose d'une police et d'un brackground-color différent pour y mettre en avant le texte.

NAVBAR:

    Notre navbar prend tout l'espace de la page quoiqu'il arrive. Une classe .disabled permet de griser l'élément non cliquable, et une classe .active permet de mettre en avant la page actuelle.

    Cette navbar est responsive et les différents <li> se positionnent l'un au dessus de l'autre lorsque la résolution de la page n'est pas assez haute.

BREADCRUMB:

    Notre breadcrumb est consitué d'un chiffre qui permet à l'utilisateur de se situer dans la structure du site sur lequel il est entrain de naviguer. Les différents items sont séparés par un ">".

    Le breadcrumb est responsive et la balise <span> qui contient le nom des élements ne s'affiche plus et met en avant seulement les numéros.

PAGER:

    Le pager est ici plutôt classique, composé d'un bouton "précédent" et "suivant" pour faciliter la navigation. Une classe .disabled s'appliquer sur l'élement "précédent" ou "suivant" lorsque l'utisateur ne peut plus naviguer entre les pages. Une classe .active met en avant la page que l'utilisateur est entrain de parcourir.

    Le pager est responsive et supprime certains espaces entre les élements pour limiter l'espace qu'il comprends.
